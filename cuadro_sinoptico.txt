

3.2 Métodos de Inferencia en Reglas
La inferencia es la forma en que obtenemos conclusiones en base de datos y declaraciones establecidas, es el 
proceso por el cual se realiza el mapeo para asignar entradas y salidas utilizando la lógica difusa. El método
 utiliza reglas tipo: si entonces (if then else)

Las reglas de inferencia son mecanismos sintácticos que permiten deducir, si: a.b.c a partir de otras a.b.c,
 existen las reglas Modus Ponens, Modus Tollens, Dilema Constructivo, Silogismo Disyuntivo, Silogismo Hipotético,
 Absorción, Adición, De Morgan, Simplificación, Conjunción, Conmutación, Asociación, Distribución, Doble negación,
 Transposición, Implicación Material, Equivalencia Material, Exportación, Tantología.













3.3 Reglas de producción
Se usan en producción y es donde se utiliza “si” y “entonces. La mayoría de los sistemas expertos utilizan sistemas
de producción.

Proposición lógica que relaciona 2 o más objetos e incluye 2 partes, la premisa y la conclusión, cada una de ellas 
contiene una expresión lógica  con una o más afirmaciones objeto-valor, conectados mediante operadores lógicos
 “AND”, “OR” y/o “NOT”

Las reglas de producción es un método procedimental de representación del conocimiento, representa y soporta las
relaciones inferenciales de un algoritmo, en contraposición a los métodos declarativos que son los hechos.

La estructura es la siguiente:

Si <antecedente>

Entonces <consecuente>

Antecedente->Consecuente

Si<condición> Entonces<conclusión, acción, etc.>

Los antecedentes son las condiciones y los consecuentes las conclusiones, acciones o hipótesis. Cada regla por si 
misma constituye un gránulo completo de conocimiento. La inferencia en los Sistemas Basados en Reglas se realiza 
mediante emparejamiento. Hay dos tipos, según el sentido:

 

·         Sistemas de encadenamiento hacia adelante: una regla es activada si los antecedentes emparejan con algunos 
hechos del sistema.

 

·         Sistemas de encadenamiento hacia atrás: una regla es activada si los consecuentes emparejan con algunos
 hechos del sistema.

 

Ventajas:

·         Uniformidad               

·         Naturalidad

·         Modularidad

·         Mantenimiento

·         Transparencia

 

Desventajas:

·         Contiene un nuevo concepto contradictorio

·         Se puede modificar las reglas

·         Su opacidad al tratar de relacionar sistemas















3.4 Sintaxis de las reglas de producción
Estructura

SI <condiciones>

ENTONCES <conclusiones, acciones, hipótesis>

Cada regla SI-ENTONCES establece un granulo completo de conocimiento

Regla_ Operador valido en un espacio de estados

CONDICIONES (premisas, precondiciones, antecedentes)

 

Formadas por clausulas y conectivas (and, or, not)

Representación causal debe corresponderse con conocimiento del dominio

Formato típico:

<Parámetro/Relación/Valor>

Parámetro: característica relevante del dominio

Relación: entre parámetro y valor

Valor: numérico, simbólico o literal

 

Conclusiones, Acciones, Hipótesis (Consecuentes)

Conclusiones, Hipótesis: son de conocimiento declarativo

Acciones: son de acción procedimental (actualiza el  conocimiento, interacción con exterior)

 

Reglas especiales

Reglas IF ALL: equivalen a reglas con las cláusulas de las condiciones conectadas con AND

Reglas IF ANY/ IF SOME: equivalen a reglas con las cláusulas de las condiciones conectadas con OR

EJEMPLO

IF: temperatura = alta

AND sudoración = presente

AND dolor_muscular = presente

THEN: diagnostico_preliminar = gripe

IF: diagnostico_preliminar = gripe

AND descompos_organos_internos = presente

THEN: diagnostico_preliminar = _abola L

 

Sistemas basados en reglas de producción

Reglas_ Operadores en búsquedas en espacio de estados

Inferencia similar al MODUS PONENS (con restricciones)

Sintaxis relajada: Se permiten acciones en los consecuentes.

Mecanismo de control determina que inferencias se pueden realizar.

 

Tipos de sistemas

En función de sintaxis de reglas y de mecanismos de control (búsqueda)

SISTEMA ENCADENAMIENTO HACIA ADELANTE (dirigidos por los datos)

Regla ACTIVADA.  Si antecedentes emparejan con algunos hechos del sistema

En IF ALL, todos.

En IF ANY, al menos uno.

Se parte de los hechos ya confirmados en el sistema

Se razona hacia adelante buscando antecedentes que emparejen

 

Sistema encadenamiento hacia atrás (dirigido por los objetivos)

Regla ACTIVADA si consecuentes emparejan con algunos hechos del sistema.

Se comienza con una hipótesis.

Se razona hacia atrás buscando consecuentes que emparejen.

 

Motor de inferencias

Elige que reglas activadas ejecutar (resolución de conflictos)

Consecuentes y antecedentes pueden verse como submetas a verificar a partir de los hechos o hipótesis, respectivamente.

 

Características

Modularidad:

Reglas = pequeñas cantidades de conocimiento (relativamente) independiente

Incrementalidad/modificabilidad: posible añadir/cambiar reglas con relativa independencia

Naturalidad y transparencia: representación del conocimiento próxima y comprensible por personas

Capacidad de generar explicaciones.

 

 

Generación de explicaciones

Posibilidad de explicar " el porqué de un resultado”

Devolver a usuario la cadena de reglas empleadas

Combinar reglas y hechos del árbol de búsqueda según las conectivas Incrementan la aceptación del resultado ofrecido
 (dominios críticos)

3.5 Semántica de las reglas de producción
Representación formal de una relación, la semántica o acción condicional es una regla que tiene la siguiente premisa:

Si

            Premisa

Entonces

            Consecuencia

Como sabemos los sistemas expertos utilizan o están basados en estas reglas de producción.

Estas reglas representan el conocimiento utilizando el formato IF-THEN (SI-ENTONCES), donde IF es el antecedente, premisa,
 condición o situación y THEN es el consecuente, conclusión, acción o respuesta.

Ofrece una factibilidad para la creación y modificación de la base del conocimiento, estos coeficientes se van utilizando 
durante el proceso de razonamiento mediante las formulas establecidas, el método asegura que entre más reglas contenga la
 semántica es más viable. Sin embargo aunque esta forma de representación es intuitiva y rápida, cuando el número de 
reglas
 es muy grande aumenta la dificultad de verificación de todas las posibilidades con el consiguiente riesgo de perder la 
coherencia lógica del sistema de base del conocimiento.

Los hechos y reglas de base de conocimiento por lo general no son exactos, es decir, tienen incertidumbre sobre el grado 
de certeza de algunos hechos y también sobre el campo de validez de las reglas.

Para el manejo de la incertidumbre en los sistemas de reglas utilizamos otros sistemas como:

·         Los factores de certeza utilizados con MYCIN: sistema experto, desarrollado en 1972 y 1980 por la universidad 
de Standford, para la realización de diagnósticos. Su objetivo radica en aconsejar a los médicos en la investigación y
 diagnóstico de las infecciones o afecciones en la sangre

·         La lógica de Dempester Shafer: extensión de la teoría de la probabilidad, para describir la incertidumbre en 
la
 evidencia. Se centra en la credibilidad de que un evento pueda ocurrir o que ya haya ocurrido, desde el punto de vista 
de una persona y de acuerdo a su toma de decisiones, esta lógica permite que la evidencia adquirida mediante la 
observación o experimentación apoye al mismo tiempo las conclusiones excluyentes o ninguna conclusión en particular. 
Fue desarrollada primero en 1967 y posteriormente retomada en 1976 debido a varias deficiencias en la teoría de la 
probabilidad

Un ejemplo es:

En una caja existen 100 objetos iguales, 30 de ellos se saben que son rojos, los 70 restantes son o azules o amarillos, 
si se toma un objeto al azar y sin ver, ¿cuál color es el que se tomará?

Este ejemplo nos dará un resultado definitivo y se puede definir mediante la inferencia, apoyado por la teoría clásica, 
teoría lógica y por la bayesiana (formulas)

·         La lógica Difusa – Fuzzy Logic: La Lógica Difusa proporciona un mecanismo de inferencia que permite simular los
 procedimientos de razonamiento humano en sistemas basados en el conocimiento.

La teoría de la lógica difusa proporciona un marco matemático que permite modelar la incertidumbre de los procesos 
cognitivos humanos de forma que pueda ser tratable por un computador.

El método de tratar la incertidumbre es una de las características más importantes de las herramientas desarrollo y se
 encuentra en discusión su validez, en su sustitución se utilizan las redes Bayesianas que tienen una matemática más
 firme, probada y viable en el actual campo de investigación. Es una lógica multivaluada que permite representar 
matemáticamente la incertidumbre y la vaguedad, proporcionando herramientas formales para su tratamiento. Básicamente, 
cualquier problema del mundo puede resolverse como dado un conjunto de variables:

·         de entrada => espacio de entrada,

·         obtener un valor adecuado de variables de salida =>espacio de salida.

La lógica difusa permite establecer este mapeo de una forma adecuada, atendiendo a criterios de significado y no de 
precisión, se utiliza en un amplio sentido, agrupando la teoría de conjunto difusos, reglas si-entonces, aritmética 
difusa y cuantificadores.